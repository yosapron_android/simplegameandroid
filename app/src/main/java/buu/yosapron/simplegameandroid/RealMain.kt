package buu.yosapron.simplegameandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import buu.yosapron.simplegameandroid.databinding.ActivityRealMainBinding

class RealMain : AppCompatActivity() {
    private lateinit var binding: ActivityRealMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityRealMainBinding>(this, R.layout.activity_real_main)
    }
}