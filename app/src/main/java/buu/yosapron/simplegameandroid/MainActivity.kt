package buu.yosapron.simplegameandroid

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.yosapron.simplegameandroid.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var checkT = 1
    private var checkF = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


//        Thread.sleep(1000)
//        PLay()

    }

    private fun PLay() {

        binding.apply {
            val Rnum1 = Rnum1
            val Rnum2 = Rnum2

            val btn1 = btn1
            val btn2 = btn2
            val btn3 = btn3

            val txtans = txtans
            val txtt = txtt
            val txtf = txtf
        }

        btn1.setBackgroundColor(Color.GRAY)
        btn2.setBackgroundColor(Color.GRAY)
        btn3.setBackgroundColor(Color.GRAY)
        btn1.isEnabled = true
        btn2.isEnabled = true
        btn3.isEnabled = true


        val pair = RandomNumAndSetText(Rnum1, Rnum2)
        val random3 = pair.first
        var ans = pair.second

        if (random3 == 1) {

            btn1.text = "${ans}"
            btn2.text = "${ans - 1}"
            btn3.text = "${ans + 1}"

            btn1.setOnClickListener {
                txtans.text = "ถูกต้อง"
                txtt.text = "${checkT++}"
                RandomNumAndSetText(Rnum1, Rnum2)
                PLay()
            }
            btn2.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn2.setBackgroundColor(Color.RED)
                btn2.isEnabled = false
            }
            btn3.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn3.setBackgroundColor(Color.RED)
                btn3.isEnabled = false
            }

        } else if (random3 == 2) {

            btn1.text = "${ans + 1}"
            btn2.text = "${ans}"
            btn3.text = "${ans - 1}"

            btn1.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn1.setBackgroundColor(Color.RED)
                btn1.isEnabled = false
            }
            btn2.setOnClickListener {
                txtans.text = "ถูกต้อง"
                txtt.text = "${checkT++}"
                RandomNumAndSetText(Rnum1, Rnum2)
                PLay()
            }
            btn3.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn3.setBackgroundColor(Color.RED)
                btn3.isEnabled = false
            }
        } else if (random3 == 3) {

            btn1.text = "${ans - 2}"
            btn2.text = "${ans + 2}"
            btn3.text = "${ans}"

            btn1.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn1.setBackgroundColor(Color.RED)
                btn1.isEnabled = false
            }
            btn2.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn2.setBackgroundColor(Color.RED)
                btn2.isEnabled = false
            }
            btn3.setOnClickListener {
                txtans.text = "ถูกต้อง"
                txtt.text = "${checkT++}"
                RandomNumAndSetText(Rnum1, Rnum2)
                PLay()
            }
        } else if (random3 == 4) {

            btn1.text = "${ans}"
            btn2.text = "${ans - 2}"
            btn3.text = "${ans + 2}"

            btn1.setOnClickListener {
                txtans.text = "ถูกต้อง"
                txtt.text = "${checkT++}"
                RandomNumAndSetText(Rnum1, Rnum2)
                PLay()
            }
            btn2.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn2.setBackgroundColor(Color.RED)
                btn2.isEnabled = false
            }
            btn3.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn3.setBackgroundColor(Color.RED)
                btn3.isEnabled = false
            }
        } else {

            btn1.text = "${ans + 1}"
            btn2.text = "${ans}"
            btn3.text = "${ans + 2}"

            btn1.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn3.setBackgroundColor(Color.RED)
                btn3.isEnabled = false
            }
            btn2.setOnClickListener {
                txtans.text = "ถูกต้อง"
                txtt.text = "${checkT++}"
                RandomNumAndSetText(Rnum1, Rnum2)
                PLay()
            }
            btn3.setOnClickListener {
                txtans.text = "ไม่ถูกต้อง"
                txtf.text = "${checkF++}"
                btn3.setBackgroundColor(Color.RED)
                btn3.isEnabled = false
            }
        }
    }

    private fun RandomNumAndSetText(
        Rnum1: TextView,
        Rnum2: TextView
    ): Pair<Int, Int> {
        val random1 = Random.nextInt(1, 10)
        val random2 = Random.nextInt(1, 10)
        val random3 = Random.nextInt(1, 5)
        Rnum1.text = "$random1"
        Rnum2.text = "$random2"
        var ans = random1 + random2
        return Pair(random3, ans)
    }
}